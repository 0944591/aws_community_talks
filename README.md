# AWS Community Talks

This repository is for people interested in giving talks to the AWS User Groups. Simply follow the [contributing guide](https://gitlab.com/ric_harvey/aws_community_talks/blob/master/CONTRIBUTING.md) and submit your talk to be considered. The talks will then be given to the User Group Leaders and your local Evangelist will help you organise the speaking dates and times.

## Template information

When filling in a *copy* of the template please fill in all ```highlighted``` values.

Most of the template is self explainitory, however you may need some guidence with selecting the type and level of your talk. Below is an explanation of both:

#### Type

- Lightning talk - 20 min talk to cover a topic of your choice.
- Live demo - This type of session is all about being practical and showing how you idea or solution works, as few slides as possible. It normally occupies a 1 hour slot.
- 1 Hour session - Full on session with powerpoint, try and include some short demos.
- Chalk Talk - No preplanned content but the willing to engage the audiance on a given topic and sketch out a solution.
- Panel - Will your talk be a panel (including customers,partners,staff).
- Other - Freeform please explain your vision.

#### Level

- Introductory - sessions are focused on providing an overview of AWS services and features, with the assumption that attendees are new to the topic. These sessions highlight basic use cases, features, functions and benefits.
- Advanced - sessions dive deeper into the selected topic. Presenters assume the audience has some familiarity with the topic, but may or may not have direct experience implementing a similar solution. Code may be shared, but will not be the primary focus of the session.
- Expert - sessions are for attendees who are deeply familiar with the topic, have implemented a solution on their own already and are comfortable with how the technology works across multiple services, architectures, and implementations. Presenters will dive into code, cover advanced tricks and explore future developments in the technology.


