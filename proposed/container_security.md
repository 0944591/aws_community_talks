# Container Security

#### Name: Ric Harvey
#### Company: AWS
#### Job Title: Technical Evangelist
#### Talk format: Live Demo
#### Level of talk: Advanced

### Elevator pitch

Live demo of how to set up and secure containers using ECR.

### In-depth pitch

Learn how to scan your docker images for security vunerabilities using Aqua and ECR.

### What will attendees will take away from your talk?

- Set up continual scanning of your repositories in ECR

### Other information

- Region: World Wide
- Speaker cert (internal only): Yes
- Speaker bio: Ric is a Technical Evangelist at AWS. He has a long career in operations and development using cloud technologies. Coupled with his passion for community and open source, he enjoys sharing this knowledge and engaging with audiences.
- Social media links: twitter: @ric__harvey gitlab: https://gitlab.com/ric_harvey
